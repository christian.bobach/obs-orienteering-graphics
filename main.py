#!/usr/bin/python3
from src.main.python.cli.cli import CLI


def main():
    cli: CLI = CLI()
    try:
        while True:
            command: str = input('>').strip()
            cli.handle_input(command)
    except (KeyboardInterrupt, EOFError):
        cli.exit()


if __name__ == '__main__':
    main()
