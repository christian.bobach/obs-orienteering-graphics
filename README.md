# obs-orienteering-graphics

Create graphics for Open Broadcast Studio with data from orienteering event 
software.

# Installation

The easiest is to use `pipenv`, this can be installed on ubuntu with the 
following command
```sudo apt install -y pipenv default-libmysqlclient-dev```

To install dependancies run
```pipenv --python 3 install```

# Run

To run the graphics cli run `./main.py` from the base dir.