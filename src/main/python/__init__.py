import datetime
from xml.etree import ElementTree

from src.main.python.application.config.config import Config

__config = Config()


def get_root_element(xml_file: str):
    tree: ElementTree.ElementTree = ElementTree.parse(xml_file)
    return tree.getroot()


def timedelta_to_str_time(seconds: float) -> str:
    time_format = __config.time_format()
    if seconds is None:
        return ''
    elif seconds < 60:
        return __int_seconds_to_str_time(seconds, '%-S')
    elif 60 < seconds < 3600:
        return __int_seconds_to_str_time(seconds, '%-M:%S')
    else:
        return __int_seconds_to_str_time(seconds, time_format)


def __int_seconds_to_str_time(seconds: float, time_format):
    _time: datetime.datetime = (datetime.datetime.min + datetime.timedelta(seconds=seconds))
    return _time.strftime(time_format)
