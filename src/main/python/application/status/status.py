from src.main.python.application.domain.class_ import Class
from src.main.python.application.domain.runner import Runner


class Status:
    class_: Class = None
    __intermediate = None
    runner: Runner = None

    def set_class(self, class_):
        self.class_ = class_

    def set_intermediate(self, description):
        self.__intermediate = description

    def set_runner(self, runner):
        self.runner = runner

    def get_status(self):
        print('Current choices:')
        if self.class_ is not None:
            print('Class:\t%s' % self.class_.name)
        if self.__intermediate is not None:
            print('Intermediate: \t%s' % self.__intermediate)
        if self.runner is not None:
            print('Runner:\t%s\t%s' % (self.runner.bib, self.runner.name))
