import os
import sys


class Config:
    # BASIC
    xml_file_key: str = 'xml_file'
    __time_format_key: str = 'time_format'

    # MeOS
    __server_key: str = 'server'
    __port_key = 'port'

    # SVG
    __svg_output_dir_key: str = 'svg_output_dir'
    __svg_list_sleep_key: str = 'svg_list_sleep'
    __svg_file_start_list_key: str = 'svg_file_start_list'
    __svg_file_intermediate_list_key = 'svg_file_intermediate_list'
    __svg_file_start_key: str = 'svg_file_start'
    __svg_file_intermediate_key = 'svg_file_intermediate'

    # EVENT
    __event_zero_time_key = 'event_zero_time'

    settings: dict = {
        # BASIC
        xml_file_key: None,
        __time_format_key: '%H:%M:%S',
        __server_key: None,
        __port_key: '2009',

        # SVG
        __svg_output_dir_key: './',
        __svg_list_sleep_key: None,
        __svg_file_start_list_key: None,
        __svg_file_intermediate_list_key: None,
        __svg_file_start_key: None,
        __svg_file_intermediate_key: None,

        # EVENT
        __event_zero_time_key: None
    }

    def __init__(self):
        self.__handle_ini_file()
        self.__handle_parameters()

    def __handle_ini_file(self):
        base_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
        relative_config_path = '/config/setup.ini'

        ini_file = open(base_dir + relative_config_path, 'r')
        for line in ini_file:
            line.strip()
            if '=' in line:
                self.__update_config(line)

    def __handle_parameters(self):
        for a in sys.argv:
            arg: str = str(a).strip()
            if arg.startswith('-') and '=' in arg:
                self.__update_config(arg[1::])

    def __update_config(self, string):
        split: str = string.split('=')
        key: str = split[0].strip()
        value: str = split[1].strip()
        self.settings.update({key: value})

    def xml_file(self) -> str:
        return str(self.settings.get(self.xml_file_key))

    def time_format(self) -> str:
        return str(self.settings.get(self.__time_format_key))

    def server(self) -> str:
        return str(self.settings.get(self.__server_key))

    def port(self):
        return str(self.settings.get(self.__port_key))

    def list_sleep(self) -> int:
        return int(self.settings.get(self.__svg_list_sleep_key))

    def svg_file_start_list(self) -> str:
        return self.settings.get(self.__svg_file_start_list_key)

    def png_file_start_list(self) -> str:
        return self.__svg_to_png_file_name(self.__svg_file_start_list_key)

    def svg_file_start(self) -> str:
        return self.settings.get(self.__svg_file_start_key)

    def png_file_start(self) -> str:
        return self.__svg_to_png_file_name(self.__svg_file_start_key)

    def svg_file_intermediate(self):
        return self.settings.get(self.__svg_file_intermediate_key)

    def png_file_intermediate(self):
        return self.__svg_to_png_file_name(self.__svg_file_intermediate_key)

    def png_file_result(self):
        return self.__svg_to_png_description_file_name(self.__svg_file_intermediate_key, 'finish')

    def event_zero_time(self):
        return self.settings.get(self.__event_zero_time_key)

    def update_event_zero_time(self, value):
        self.settings.update({self.__event_zero_time_key: value})

    def svg_file_intermediate_list(self):
        return self.settings.get(self.__svg_file_intermediate_list_key)

    def png_file_intermediate_list(self):
        return self.__svg_to_png_file_name(self.__svg_file_intermediate_list_key)

    def png_file_result_list(self):
        return self.__svg_to_png_description_file_name(self.__svg_file_intermediate_list_key, 'finish')

    def __svg_to_png_description_file_name(self, key, description):
        path = self.settings.get(self.__svg_output_dir_key)
        file = str(self.settings.get(key)).split('/')[-1]
        return path + description + file + '.png'

    def __svg_to_png_file_name(self, key):
        path = self.settings.get(self.__svg_output_dir_key)
        file = str(self.settings.get(key)).split('/')[-1]
        return path + file + '.png'
