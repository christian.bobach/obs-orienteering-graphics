from src.main.python.application.domain.runner import Runner
from src.main.python.application.service.meos_service import MeosService
from src.main.python.application.service.writer.individual_writer.individual_writer import IndividualWriter


def get_runner(status):
    runner = status.runner
    if runner is None:
        print('Runner should be chosen before intermediate.')
    else:
        return runner


def sync_runner(status, runner: Runner, finish_runner):
    if runner.place is None:
        runner.place = finish_runner.place
    if runner.time is None:
        runner.time = finish_runner.time
    if runner.time_diff is None:
        runner.time_diff = finish_runner.time_diff
    if runner.status is None:
        runner.status = finish_runner.status
    status.set_runner(runner)


class IndividualWriterService:
    __meos_service = MeosService()
    __individual_writer = None

    def write(self, status, bib):
        start_runners = self.__meos_service.get_runners()
        start_runner = next(runner for runner in start_runners if runner.bib == bib)

        individual_writer = self.__get_singleton_individual_writer()
        individual_writer.handle_start_writer(start_runner)

        description = 'Finish'
        finish_runners = self.__meos_service.get_runners_for_intermediate(description)
        finish_runner = next(runner for runner in finish_runners if runner.id == start_runner.id)
        sync_runner(status, start_runner, finish_runner)

        individual_writer.handle_result_writer(description, finish_runner)

    def write_intermediate(self, status, description):
        runner = get_runner(status)
        individual_writer = self.__get_singleton_individual_writer()
        individual_writer.handle_intermediate_writer(description, runner)

    def __get_singleton_individual_writer(self):
        if self.__individual_writer is None:
            self.__individual_writer = IndividualWriter()
        return self.__individual_writer
