from urllib import request
from xml.etree import ElementTree

from src.main.python.application.domain.runner import Runner
from src.main.python.application.service.dataaccess.meos.rest import meos_endpoint, meos_encoding, find_all, find


def extract_name(person, runner):
    name: ElementTree.Element = find('name', person)
    runner.id = name.attrib['id']
    runner.name = name.text


def extract_time(person, runner):
    try:
        runner.time = int(person.attrib['rt']) / 10
    except KeyError:
        runner.time = None


def extract_place(person, runner):
    try:
        runner.place = int(person.attrib['place'])
    except KeyError:
        runner.place = None


class ResultsFacade:

    def __init__(self, classes_facade, organisations_facade):
        self.classes_facade = classes_facade
        self.organisations_facade = organisations_facade

    def get_results(self, to):
        return self.__get_results(to)

    def __get_results(self, to):
        url = '%s?get=result&to=%s' % (meos_endpoint, to)
        response = request.urlopen(url)
        xml = response.read().decode(meos_encoding)
        return self.__extract_persons(xml)

    def __extract_persons(self, xml):
        root = ElementTree.fromstring(xml)
        result = find('results', root)
        persons = find_all('person', result)

        runners = []
        for person in persons:
            runner = self.__extract_person(person)
            runners.append(runner)

        return runners

    def __extract_person(self, person: ElementTree.Element):
        runner = Runner()
        extract_time(person, runner)
        extract_place(person, runner)
        runner.status = person.attrib['stat']
        runner.class_ = self.classes_facade.get_class(person.attrib['cls'])

        extract_name(person, runner)
        self.__extract_org(person, runner)

        return runner

    def __extract_org(self, person, runner):
        org: ElementTree.Element = find('org', person)
        org_id = org.attrib['id']
        runner.club = self.organisations_facade.get_organisation(org_id)
