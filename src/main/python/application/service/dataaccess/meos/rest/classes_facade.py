from urllib import request
from xml.etree import ElementTree

from src.main.python.application.domain.class_ import Class
from src.main.python.application.service.dataaccess.meos.rest import meos_get_path, meos_encoding, find_all


def extract_class(cls: ElementTree.Element):
    class_ = Class()
    class_.id = cls.attrib['id']
    class_.name = cls.text
    class_.radios = str(cls.attrib['radio']).split(',')
    return class_


def extract_classes(xml_classes: ElementTree.Element):
    root = ElementTree.fromstring(xml_classes)
    clss = find_all('cls', root)

    classes = []
    for cls in clss:
        class_ = extract_class(cls)
        classes.append(class_)

    return classes


def get_classes():
    url = meos_get_path('class')
    response = request.urlopen(url)
    xml_classes = response.read().decode(meos_encoding)
    return extract_classes(xml_classes)


class ClassesFacade:
    __classes = None

    def get_classes(self):
        return self.__get_singleton_classes()

    def get_class(self, id_):
        try:
            classes = self.__get_singleton_classes()
            return next(class_ for class_ in classes if class_.id == id_)
        except StopIteration:
            return '-'

    def __get_singleton_classes(self):
        if self.__classes is None:
            self.__classes = get_classes()
        return self.__classes
