from urllib import request
from xml.etree import ElementTree

from src.main.python.application.domain.club import Club
from src.main.python.application.service.dataaccess.meos.rest import find_all, meos_encoding, meos_get_path


def extract_club(org: ElementTree.Element):
    club = Club()
    club.id = org.attrib['id']
    club.name = org.text
    return club


def extract_org(xml):
    root = ElementTree.fromstring(xml)
    orgs = find_all('org', root)

    clubs = []
    for org in orgs:
        club = extract_club(org)
        clubs.append(club)

    return clubs


def get_orginazations():
    url = meos_get_path('organization')
    response = request.urlopen(url)
    xml = response.read().decode(meos_encoding)
    return extract_org(xml)


class OrganisationsFacade:
    __organisations = None

    def get_organisation(self, id_):
        try:
            clubs = self.__get_singleton_organisations()
            return next(club for club in clubs if club.id == id_)
        except StopIteration:
            return Club()

    def __get_singleton_organisations(self):
        if self.__organisations is None:
            self.__organisations = get_orginazations()
        return self.__organisations
