from xml.etree import ElementTree

from src.main.python import Config

__config = Config()
meos_endpoint = 'http://%s:%s/meos' % (__config.server(), __config.port())
meos_namespace = '{http://www.melin.nu/mop}'
meos_encoding = 'utf-8'


def find_all(tag, root: ElementTree.Element):
    return root.findall('%s%s' % (meos_namespace, tag))


def find(tag, cmp):
    return cmp.find('%s%s' % (meos_namespace, tag))


def meos_get_path(param):
    return '%s?get=%s' % (meos_endpoint, param)
