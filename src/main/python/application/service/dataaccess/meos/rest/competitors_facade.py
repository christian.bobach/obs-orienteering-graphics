from urllib import request
from xml.etree import ElementTree

from src.main.python.application.domain.runner import Runner
from src.main.python.application.service.dataaccess.meos.rest import find_all, meos_endpoint, meos_encoding, find


class CompetitorsFacade:
    __competitors = None

    def __init__(self, classes_facade, organisations_facade):
        self.classes_facade = classes_facade
        self.organisations_facade = organisations_facade

    def get_competitors(self):
        return self.__get_singleton_competitors()

    def __get_singleton_competitors(self):
        if self.__competitors is None:
            self.__competitors = self.__get_competitors()
        return self.__competitors

    def __get_competitors(self):
        url = '%s?get=competitor' % meos_endpoint
        response = request.urlopen(url)
        xml = response.read().decode(meos_encoding)
        return self.__extract_competitors(xml)

    def __extract_competitors(self, xml):
        root = ElementTree.fromstring(xml)
        cmps = find_all('cmp', root)

        runners = []
        for cmp in cmps:
            runner = self.__extract_competitor(cmp)
            runners.append(runner)

        return runners

    def __extract_competitor(self, cmp: ElementTree.Element):
        runner = Runner()
        runner.id = cmp.attrib['id']

        return self.__extract_base(cmp, runner)

    def __extract_base(self, cmp, runner):
        cmp_base: ElementTree.Element = find('base', cmp)
        runner.bib = cmp_base.attrib['bib']
        runner.name = cmp_base.text
        runner.club = self.organisations_facade.get_organisation(cmp_base.attrib['org'])
        runner.class_ = self.classes_facade.get_class(cmp_base.attrib['cls'])
        runner.start_time = int(cmp_base.attrib['st']) / 10
        return runner
