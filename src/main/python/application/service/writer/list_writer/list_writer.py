from src.main.python.application.config.config import Config
from src.main.python.application.service.writer import stop_writer
from src.main.python.application.service.writer.list_writer.intermediate_list_writer import IntermediateListWriter
from src.main.python.application.service.writer.list_writer.start_list_writer import StartListWriter


class ListWriter:
    __config = Config()

    __start_list_writer: StartListWriter = None
    __result_list_writer: IntermediateListWriter = None
    __intermediate_list_writer: IntermediateListWriter = None

    def handle_start_list_writer(self, class_, runners):
        stop_writer(self.__start_list_writer)
        svg_file = self.__config.svg_file_start_list()
        png_file = self.__config.png_file_start_list()
        self.__start_list_writer = StartListWriter(class_, runners, svg_file, png_file)
        self.__start_list_writer.start()

    def handle_result_list_writer(self, description, class_, runners):
        stop_writer(self.__result_list_writer)
        svg_file = self.__config.svg_file_intermediate_list()
        png_file = self.__config.png_file_result_list()
        self.__result_list_writer = IntermediateListWriter(description, class_, runners, svg_file, png_file)
        self.__result_list_writer.start()

    def handle_intermediate_list_writer(self, description, class_, runners):
        stop_writer(self.__result_list_writer)
        svg_file = self.__config.svg_file_intermediate_list()
        png_file = self.__config.png_file_intermediate_list()
        self.__result_list_writer = IntermediateListWriter(description, class_, runners, svg_file, png_file)
        self.__result_list_writer.start()

    def stop(self):
        print('Stopping class writers.')
        stop_writer(self.__start_list_writer)
        stop_writer(self.__result_list_writer)
        stop_writer(self.__intermediate_list_writer)
