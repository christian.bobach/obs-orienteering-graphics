from src.main.python import get_root_element
from src.main.python.application.service.writer.list_writer.abstract_list_writer import AbstractListWriter


class StartListWriter(AbstractListWriter):
    def __init__(self, class_, runners, svg_file, png_file):
        super(StartListWriter, self).__init__()
        self.class_ = class_
        self.runners: list = [runner for runner in runners if runner.class_.id == class_.id]
        self.runners.sort(key=lambda runner: runner.bib)
        self.runners.sort(key=lambda runner: runner.start_time)

        self.svg_root = get_root_element(svg_file)
        self.png_file: str = png_file

    def run(self):
        super(StartListWriter, self).run()
