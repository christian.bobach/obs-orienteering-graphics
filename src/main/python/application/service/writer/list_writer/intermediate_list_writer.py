from src.main.python import get_root_element
from src.main.python.application.domain.runner import Runner
from src.main.python.application.service.writer.list_writer.abstract_list_writer import AbstractListWriter


def update_time_diff(runners):
    best_runner: Runner = runners[0]

    for runner in runners:
        if runner.time is not None:
            runner.time_diff = runner.time - best_runner.time


class IntermediateListWriter(AbstractListWriter):
    def __init__(self, description, class_, runners, svg_file, png_file):
        super(IntermediateListWriter, self).__init__()
        self.class_ = class_
        self.description = description
        self.runners = [runner for runner in runners if runner.class_.id == class_.id]
        self.runners.sort(key=lambda runner: (runner.place is None, runner.place))

        self.svg_root = get_root_element(svg_file)
        self.png_file = png_file

        if len(self.runners) > 0:
            update_time_diff(self.runners)

    def run(self):
        super(IntermediateListWriter, self).run()
