from src.main.python.application.domain.runner import Runner
from src.main.python.application.service.writer import update_runner, get_pages, write_pages, find_element, \
    update_class, update_description
from src.main.python.application.service.writer.thread.stoppable_thread import StoppableThread


class AbstractListWriter(StoppableThread):
    description = None
    class_ = None
    runners = None
    png_file = None
    svg_root = None

    def __init__(self):
        super(AbstractListWriter, self).__init__()

    def run(self):
        if self.svg_root is not None:
            self.__write()

    def __write(self):
        svg_class = find_element('class', self.svg_root)
        update_class(self.class_, svg_class)

        update_description(self.description, self.svg_root)

        pages: list = get_pages('runner', update_runner, Runner, self.svg_root, self.runners)
        if len(pages) is not 0:
            write_pages(self, pages, self.png_file)
