import threading


class StoppableThread(threading.Thread):
    def __init__(self):
        super(StoppableThread, self).__init__()
        self.__stop_event: threading.Event = threading.Event()

    def running(self):
        return not self.__stop_event.is_set()

    def stop(self):
        self.__stop_event.set()
