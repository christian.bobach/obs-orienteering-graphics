from src.main.python.application.service.writer import find_element, update_class, update_runner, write_element_to_png, \
    update_description


class AbstractIndividualWriter:
    description = None
    runner = None
    svg_root = None
    png_file = None

    def write(self):
        if self.svg_root is not None:
            svg_class = find_element('class', self.svg_root)
            update_class(self.runner.class_, svg_class)

            update_description(self.description, self.svg_root)

            svg_runner = find_element('runner', self.svg_root)
            update_runner(self.runner, svg_runner)

            write_element_to_png(self.svg_root, self.png_file)
