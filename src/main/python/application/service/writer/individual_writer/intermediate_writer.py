from src.main.python import get_root_element
from src.main.python.application.service.writer.individual_writer.abstract_individual_writer import \
    AbstractIndividualWriter


class IntermediateWriter(AbstractIndividualWriter):
    def __init__(self, description, runner, svg_file, png_file):
        super().__init__()
        self.png_file = png_file
        self.svg_root = get_root_element(svg_file)
        self.runner = runner
        self.description = description

    def write(self):
        super().write()
