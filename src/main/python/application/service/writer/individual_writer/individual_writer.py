from src.main.python.application.config.config import Config
from src.main.python.application.service.writer.individual_writer.intermediate_writer import IntermediateWriter
from src.main.python.application.service.writer.individual_writer.start_writer import StartWriter


class IndividualWriter:
    __config = Config()

    def handle_start_writer(self, start_runner):
        svg_file = self.__config.svg_file_start()
        png_file = self.__config.png_file_start()
        StartWriter(start_runner, svg_file, png_file).write()

    def handle_result_writer(self, description, runner):
        svg_file = self.__config.svg_file_intermediate()
        png_file = self.__config.png_file_result()
        IntermediateWriter(description, runner, svg_file, png_file).write()

    def handle_intermediate_writer(self, description, runner):
        svg_file = self.__config.svg_file_intermediate()
        png_file = self.__config.png_file_intermediate()
        IntermediateWriter(description, runner, svg_file, png_file).write()
