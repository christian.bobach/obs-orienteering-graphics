from src.main.python import get_root_element
from src.main.python.application.service.writer.individual_writer.abstract_individual_writer import \
    AbstractIndividualWriter


class StartWriter(AbstractIndividualWriter):
    def __init__(self, runner, svg_file, png_file):
        super().__init__()
        self.runner = runner

        self.svg_root = get_root_element(svg_file)
        self.png_file = png_file

    def write(self):
        super().write()
