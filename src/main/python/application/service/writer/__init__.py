import copy
import datetime
import math
import time
from xml.etree import ElementTree

from cairosvg import svg2png

from src.main.python import timedelta_to_str_time
from src.main.python.application.config.config import Config
from src.main.python.application.domain.class_ import Class
from src.main.python.application.domain.runner import Runner
from src.main.python.application.service.writer.thread.stoppable_thread import StoppableThread

__config: Config = Config()


def stop_writer(writer: StoppableThread):
    if writer is not None and writer.running():
        writer.stop()


def write_element_to_png(root: ElementTree.Element, png_file: str):
    bs: bytes = ElementTree.tostring(root, 'UTF-8')
    svg2png(bytestring=bs, write_to=png_file)


def write_pages(writer: StoppableThread, pages: list, file: str):
    i: int = 0
    while writer.running():
        index: int = i % len(pages)
        page = pages[index]
        write_element_to_png(page, file)
        i += 1
        time.sleep(__config.list_sleep())


def str_timedeltas_to_str_time(seconds1: str, seconds2: str) -> str:
    timedelta1: datetime.timedelta = datetime.timedelta(seconds=float(seconds1))
    timedelta2: datetime.timedelta = datetime.timedelta(seconds=float(seconds2))
    _time: datetime.datetime = (datetime.datetime.min + timedelta1 + timedelta2)
    return _time.strftime(__config.time_format())


def __get_query(attribute_value):
    return ".//*[@class='" + attribute_value + "']"


def find_elements(attribute_value: str, root: ElementTree.Element) -> list:
    query = __get_query(attribute_value)
    return root.findall(query)


def find_element(attribute_value: str, root: ElementTree.Element) -> ElementTree.Element:
    query = __get_query(attribute_value)
    return root.find(query)


def update_element(attribute_value: str, element_value, root: ElementTree.Element):
    element: ElementTree.Element = find_element(attribute_value, root)
    if element is not None:
        element.text = str(element_value)


def update_element_safe(attribute_value: str, element_value, element: ElementTree.Element):
    if element_value is None:
        update_element(attribute_value, '', element)
    else:
        update_element(attribute_value, element_value, element)


def update_class(class_: Class, element: ElementTree.Element):
    update_element_safe('name', class_.name, element)


def update_description(description: str, element: ElementTree.Element):
    update_element_safe('description', description, element)


def update_runner(runner: Runner, element: ElementTree.Element):
    update_element_safe('place', runner.place, element)
    update_element_safe('bib', runner.bib, element)
    update_element_safe('name', runner.name, element)
    update_element_safe('start_time', timedelta_to_str_time(runner.start_time), element)
    __update_time(element, runner)
    update_time_diff(element, runner)
    update_element_safe('club_name', runner.club.name, element)


def __update_time(element, runner):
    attribute_value = 'time'
    if runner.time is not None:
        update_element(attribute_value, timedelta_to_str_time(runner.time), element)
    elif runner.status is not None and int(runner.status) > 1:
        update_element(attribute_value, 'DSQ', element)
    else:
        update_element(attribute_value, '', element)


def update_time_diff(element, runner):
    diff = runner.time_diff
    locator = 'time_diff'
    if diff is None or int(diff) is 0:
        update_element(locator, '', element)
    else:
        time_diff = '+%s' % timedelta_to_str_time(diff)
        update_element(locator, time_diff, element)


def get_num_pages(elements, list_):
    return int(math.ceil(len(list_) / len(elements)))


def get_pages(search_element: str,
              update_func: staticmethod,
              default_object: object,
              root: ElementTree.Element,
              list_: list):
    elements: list = find_elements(search_element, root)
    num_pages = get_num_pages(elements, list_)

    pages: list = []
    for i in range(0, num_pages):
        page: ElementTree.Element = get_page(i, root, list_, search_element, update_func, default_object)
        pages.append(page)
    return pages


def get_page(page: int,
             root: ElementTree.Element,
             list_: list,
             search_element: str,
             update_func: staticmethod,
             default_obj: object) -> ElementTree.Element:
    root_copy: ElementTree.Element = copy.deepcopy(root)
    svg_list: list = find_elements(search_element, root_copy)
    svg_list_size: int = len(svg_list)
    for j in range(0, svg_list_size):
        svg_element: ElementTree.Element = svg_list[j]
        index: int = page * svg_list_size + j
        if index < len(list_):
            obj: object = list_[index]
            update_func(obj, svg_element)
        else:
            update_func(default_obj, svg_element)
    return root_copy
