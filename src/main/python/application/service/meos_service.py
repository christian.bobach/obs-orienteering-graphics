from urllib.error import URLError

from src.main.python.application.service.dataaccess.meos.rest.classes_facade import ClassesFacade
from src.main.python.application.service.dataaccess.meos.rest.competitors_facade import CompetitorsFacade
from src.main.python.application.service.dataaccess.meos.rest.organisations_facade import OrganisationsFacade
from src.main.python.application.service.dataaccess.meos.rest.results_facade import ResultsFacade


class MeosService:
    __classes_facade = ClassesFacade()
    __organisations_facade = OrganisationsFacade()
    __competitors_facade = CompetitorsFacade(__classes_facade, __organisations_facade)
    __results_facade = ResultsFacade(__classes_facade, __organisations_facade)

    def get_classes(self):
        return self.__classes_facade.get_classes()

    def get_runners(self):
        try:
            return self.__competitors_facade.get_competitors()
        except URLError:
            print('No route to host')

    def get_runners_for_intermediate(self, to):
        return self.__results_facade.get_results(to)
