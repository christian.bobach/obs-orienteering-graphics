from src.main.python.application.service.meos_service import MeosService
from src.main.python.application.service.writer.list_writer.list_writer import ListWriter


def get_class(status):
    class_ = status.class_
    runner = status.runner
    if class_ is None and runner is None:
        print('Class should be chosen before intermediate.')
    elif class_ is None:
        return runner.class_
    else:
        return class_


class ListWriterService:
    __meos_service = MeosService()
    __list_writer = None

    def write_lists(self, status, id_):
        class_ = next(class_ for class_ in self.__meos_service.get_classes() if class_.id == id_)
        status.set_class(class_)

        list_writer = self.__get_singleton_list_writer()
        start_list_runners = self.__meos_service.get_runners()
        list_writer.handle_start_list_writer(class_, start_list_runners)

        description = 'Finish'
        intermediate_list_runners = self.__meos_service.get_runners_for_intermediate(description)
        list_writer.handle_result_list_writer(description, class_, intermediate_list_runners)

    def write_intermediate(self, status, description):
        class_ = get_class(status)
        status.set_intermediate(description)
        list_writer = self.__get_singleton_list_writer()
        list_writer.handle_intermediate_list_writer(description, class_,
                                                    self.__meos_service.get_runners_for_intermediate(description))

    def stop(self):
        if self.__list_writer is not None:
            self.__list_writer.stop()

    def __get_singleton_list_writer(self):
        if self.__list_writer is None:
            self.__list_writer = ListWriter()
        return self.__list_writer
