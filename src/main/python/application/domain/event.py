class Event:
    id: str = None
    name: str = None
    date: str = None
    zero_time: int = None
    database: str = None
