from src.main.python.application.domain.class_ import Class
from src.main.python.application.domain.club import Club


class Runner:
    id: int = None
    name: str = None
    bib: str = None
    start_time: int = None
    place: int = None
    time: int = None
    time_diff: int = None
    status: int = None
    club: Club = Club()
    class_: Class = Class()
