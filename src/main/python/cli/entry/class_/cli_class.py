from src.main.python.application.service.list_writer_service import ListWriterService
from src.main.python.application.status.status import Status
from src.main.python.cli import is_correct_input_key, get_command_value
from src.main.python.cli.entry.cli_entry import CLIEntry


class CLIClass(CLIEntry):
    __command_class = 'c'
    __command_class_argument = '[id|name]'

    def __init__(self, status: Status):
        self.status = status

    def manual(self):
        print('\t%s %s:\tselect the class specified if in event.'
              % (self.__command_class, self.__command_class_argument))

    def handle_input(self, command: str):
        if is_correct_input_key(self.__command_class, command):
            id_: str = get_command_value(command)
            try:
                ListWriterService().write_lists(self.status, id_)
            except StopIteration:
                print('Could not determine class with id: %s.' % str(id_))
            return True

    def exit(self):
        ListWriterService().stop()
