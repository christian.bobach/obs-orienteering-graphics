from src.main.python.application.service.meos_service import MeosService
from src.main.python.cli import is_correct_input_key, human_string_sort
from src.main.python.cli.entry.cli_entry import CLIEntry


class CLIClassList(CLIEntry):
    __command_class_list = 'c list'

    def manual(self):
        print('\t%s:\t\t\tlist all classes in event.' % self.__command_class_list)

    def handle_input(self, command: str):
        if is_correct_input_key(self.__command_class_list, command):
            classes = MeosService().get_classes()
            classes.sort(key=lambda clazz: human_string_sort(str(clazz.name)))

            print('id\tname')
            for class_ in classes:
                print('%s\t%s' % (class_.id, class_.name))
            return True

    def exit(self):
        pass
