import abc
from abc import abstractmethod


class CLIEntry(abc.ABC):
    @abstractmethod
    def manual(self):
        raise NotImplementedError

    @abstractmethod
    def handle_input(self, command: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def exit(self):
        pass
