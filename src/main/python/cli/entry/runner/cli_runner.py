from src.main.python.application.service.individual_writer_service import IndividualWriterService
from src.main.python.application.status.status import Status
from src.main.python.cli import is_correct_input_key, get_command_value
from src.main.python.cli.entry.cli_entry import CLIEntry


class CLIRunner(CLIEntry):
    __command_runner = 'r'
    __command_runner_argument = '[bib]'

    def __init__(self, status: Status):
        self.status = status

    def manual(self):
        print('\t%s %s:\t\tselect the runner specified if in event.'
              % (self.__command_runner, self.__command_runner_argument))

    def handle_input(self, command: str):
        if is_correct_input_key('r', command):
            bib = get_command_value(command)
            try:
                IndividualWriterService().write(self.status, bib)
            except StopIteration:
                print('Could not determine runner with bib: %s' % bib)
            return True

    def exit(self):
        pass
