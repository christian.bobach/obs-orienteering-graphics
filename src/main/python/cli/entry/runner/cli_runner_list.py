from src.main.python.application.service.meos_service import MeosService
from src.main.python.application.status.status import Status
from src.main.python.cli import is_correct_input_key, human_string_sort
from src.main.python.cli.entry.cli_entry import CLIEntry


class CLIRunnerList(CLIEntry):
    __command_runner_list = 'r list'

    def __init__(self, status: Status):
        self.status = status

    def manual(self):
        print('\t%s:\t\t\tlist all runners in chosen class.' % self.__command_runner_list)

    def handle_input(self, command: str):
        if is_correct_input_key(self.__command_runner_list, command):
            runners = MeosService().get_runners()
            runners.sort(key=lambda r: human_string_sort(r.bib))

            print('bib\tname')
            for runner in runners:
                print('%s\t%s' % (runner.bib, runner.name))
            return True

    def exit(self):
        pass
