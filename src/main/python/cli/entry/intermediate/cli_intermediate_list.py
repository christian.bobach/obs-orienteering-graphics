from src.main.python.application.status.status import Status
from src.main.python.cli import is_correct_input_key
from src.main.python.cli.entry.cli_entry import CLIEntry


def print_intermediates(class_):
    print('intermediate')
    for intermediate in class_.radios:
        print(intermediate)


class CLIIntermediateList(CLIEntry):
    __command_intermediate_list = 'i list'

    def __init__(self, status: Status):
        self.status = status

    def manual(self):
        print('\t%s\t\t\tlist all intermediates in chosen class.' % self.__command_intermediate_list)

    def handle_input(self, command: str) -> bool:
        if is_correct_input_key(self.__command_intermediate_list, command):
            class_ = self.status.class_
            runner = self.status.runner
            if class_ is None and runner is None:
                print('A class or runner must be selected.')
            elif class_ is None:
                print_intermediates(runner.class_)
            else:
                print_intermediates(class_)
            return True

    def exit(self):
        pass
