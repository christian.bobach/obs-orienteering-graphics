from _elementtree import ParseError

from src.main.python.application.service.individual_writer_service import IndividualWriterService
from src.main.python.application.service.list_writer_service import ListWriterService
from src.main.python.application.status.status import Status
from src.main.python.cli import is_correct_input_key, get_command_value
from src.main.python.cli.entry.cli_entry import CLIEntry


class CLIIntermediate(CLIEntry):
    __command_intermediate = 'i'
    __command_intermediate_argument = 'id'

    def __init__(self, status: Status):
        self.status = status

    def manual(self):
        print('\t%s %s\t\t\tselect the intermediate specified if in selected class'
              % (self.__command_intermediate, self.__command_intermediate_argument))

    def handle_input(self, command: str) -> bool:
        if is_correct_input_key(self.__command_intermediate, command):
            description = get_command_value(command)
            try:
                IndividualWriterService().write_intermediate(self.status, description)
                ListWriterService().write_intermediate(self.status, description)
            except ParseError:
                print('Could not determine intermediate from context, description: %s' % description)
            return True

    def exit(self):
        ListWriterService().stop()
