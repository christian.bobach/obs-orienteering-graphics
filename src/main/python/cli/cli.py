import sys

from src.main.python.application.status.status import Status
from src.main.python.cli import is_help, is_status, is_exit
from src.main.python.cli.entry.class_.cli_class import CLIClass
from src.main.python.cli.entry.class_.cli_class_list import CLIClassList
from src.main.python.cli.entry.cli_entry import CLIEntry
from src.main.python.cli.entry.intermediate.cli_intermediate import CLIIntermediate
from src.main.python.cli.entry.intermediate.cli_intermediate_list import CLIIntermediateList
from src.main.python.cli.entry.runner.cli_runner import CLIRunner
from src.main.python.cli.entry.runner.cli_runner_list import CLIRunnerList

separator: str = '----------------------------------'


class CLI(CLIEntry):
    __clis: list = []
    __status: Status = Status()

    def __init__(self):
        cli_class_list = CLIClassList()
        self.__clis.append(cli_class_list)
        cli_class = CLIClass(self.__status)
        self.__clis.append(cli_class)

        cli_intermediate_list = CLIIntermediateList(self.__status)
        self.__clis.append(cli_intermediate_list)
        cli_intermediate = CLIIntermediate(self.__status)
        self.__clis.append(cli_intermediate)

        cli_runner_list = CLIRunnerList(self.__status)
        self.__clis.append(cli_runner_list)
        cli_runner = CLIRunner(self.__status)
        self.__clis.append(cli_runner)

    def manual(self):
        print('This CLI should be used as follows:')
        print()
        for cli in self.__clis:
            cli.manual()
            print()
        print('To get help type [help|man|manual]')
        print()
        print('To exit type [exit|q|quit].')
        print(separator)

    def status(self):
        print(separator)
        self.__status.get_status()
        print(separator)

    def handle_input(self, command):
        if is_help(command):
            self.manual()
        elif is_status(command):
            self.status()
        elif is_exit(command):
            self.exit()
        else:
            self.__handle_input(command)

    def __handle_input(self, command):
        handled_input: bool = False
        for cli in self.__clis:
            handled_input = cli.handle_input(command)

            if handled_input:
                self.status()
                break

        if not handled_input:
            self.__handle_non_recognised_input(command)

    def exit(self):
        for cli in self.__clis:
            cli.exit()
        print(separator)
        print('Hope you had a nice experience. Farewell!')
        sys.exit(1)

    def __handle_non_recognised_input(self, command):
        print(separator)
        print('Could not recognise input [%s].' % str(command))
        print()
        self.manual()
