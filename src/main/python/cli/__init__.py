import re


def is_correct_input_key(key: str, command: str) -> bool:
    """
    Test for correct input command compared with input key.

    :param key: string to test against.
    :param command: given as input.
    :return: true if command contains key and another argument.
    """
    return command.strip().startswith(key) and ' ' in command


def get_command_value(command: str) -> str:
    """
    Get the value of the command inputted.

    :param command: to be handled.
    :return: the value associated with the command.
    """
    commands: list = command.split(' ', 1)
    value: str = commands[1]
    return value.strip()


def is_help(command: str) -> bool:
    """
    Decides if a command is a help command.

    :param command: that will be tested.
    :return: true if command is [h|help|man|manual] else false.
    """
    return command.strip().startswith('h') \
           or command.strip().startswith('help') \
           or command.strip().startswith('man') \
           or command.strip().startswith('manual')


def is_status(command: str) -> bool:
    """
    Decides if a command is a help command.

    :param command: that will be tested.
    :return: true if command is [s|status] else false.
    """
    return command.strip().startswith('s') \
           or command.strip().startswith('status')


def is_exit(command: str) -> bool:
    """
    Decides if a command is a kill command.

    :param command: that will be tested.
    :return: true if command is [q|quit|exit] else false.
    """
    return command.strip().startswith('q') \
           or command.strip().startswith('quit') \
           or command.strip().startswith('exit')


def human_string_sort(text):
    return [__atoi(c) for c in re.split(r'(\d+)', text)]


def __atoi(text):
    return int(text) if text.isdigit() else text
