#!/usr/bin/python3
import unittest
from xml.etree import ElementTree

from src.main.python.application.domain.class_ import Class
from src.main.python.application.domain.course import Course
from src.main.python.application.service.writer import find_element, update_element, update_element_safe, get_pages, \
    update_class


def __get_stub_sub_element(value) -> ElementTree.Element:
    element = ElementTree.Element('test-sub-element')
    element.set('class', value)
    element.text = ''
    return element


def get_stub_simple_root(value: str) -> ElementTree.Element:
    element = __get_stub_sub_element(value)
    root = ElementTree.Element('test-element')
    root.append(element)

    return root


def get_stub_complex_root() -> ElementTree.Element:
    name_element = __get_stub_sub_element('name')
    num_controls_element = __get_stub_sub_element('num_controls')
    length_element = __get_stub_sub_element('length')
    climb_element = __get_stub_sub_element('climb')

    class_element = ElementTree.Element('test-class-element')
    class_element.set('class', 'class')
    class_element.append(name_element)
    class_element.append(num_controls_element)
    class_element.append(length_element)
    class_element.append(climb_element)

    root = ElementTree.Element('test-root')
    root.append(class_element)
    return root


def __get_stub_course(num_controls: int, climb: int, length: int) -> Course:
    course = Course()
    course.num_controls = num_controls
    course.climb = climb
    course.length = length
    return course


def __get_stub_class(name: str, num_controls: int, climb: int, length: int) -> Class:
    course = __get_stub_course(num_controls, climb, length)
    class_ = Class()
    class_.name = name
    class_.course = course
    return class_


def get_stub_classes() -> list:
    classes = [__get_stub_class('M21', 21, 543, 12345),
               __get_stub_class('D21', 18, 345, 8264),
               __get_stub_class('D12', 3, 123, 2345)]
    return classes


class TestClassListWriter(unittest.TestCase):
    def test_update_element(self):
        value = 'test'
        text = 'text'
        root = get_stub_simple_root(value)
        element: ElementTree.Element = find_element(value, root)
        self.assertEqual('', element.text)

        update_element(value, text, root)
        updated_element: ElementTree.Element = find_element(value, root)
        self.assertEqual(text, updated_element.text)

    def test_update_name(self):
        value = 'name'
        name = 'test_name'
        root = get_stub_simple_root(value)
        update_element_safe(value, name, root)
        element: ElementTree.Element = find_element(value, root)

        self.assertEqual(name, element.text)

    def test_update_num_controls(self):
        value = 'num_controls'
        num_controls = 3
        root = get_stub_simple_root(value)
        update_element_safe(value, num_controls, root)
        element: ElementTree.Element = find_element(value, root)

        self.assertEqual(str(num_controls), element.text)

    def test_update_length(self):
        value = 'length'
        length = 10298
        root = get_stub_simple_root(value)
        update_element_safe(value, length, root)
        element: ElementTree.Element = find_element(value, root)

        self.assertEqual(str(length), element.text)

    def test_update_climb(self):
        value = 'climb'
        climb = 367
        root = get_stub_simple_root(value)
        update_element_safe(value, climb, root)
        element: ElementTree.Element = find_element(value, root)

        self.assertEqual(str(climb), element.text)

    def test_get_class_pages(self):
        root: ElementTree.Element = get_stub_complex_root()
        classes: list = get_stub_classes()

        pages = get_pages('class', update_class, Class, root, classes)
        self.assertEqual(3, len(pages))
        self.__assert_class(pages[0], 'M21', 21, 12345, 543)
        self.__assert_class(pages[1], 'D21', 18, 8264, 345)

    def __assert_class(self, class_page: ElementTree.Element, name: str, num_controls: int, length: int, climb: int):
        name_element = find_element('name', class_page)
        self.assertEqual(name, name_element.text)
        num_control_element = find_element('num_controls', class_page)
        self.assertEqual(str(num_controls), num_control_element.text)
        length_element = find_element('length', class_page)
        self.assertEqual(str(length), length_element.text)
        climb_element = find_element('climb', class_page)
        self.assertEqual(str(climb), climb_element.text)
